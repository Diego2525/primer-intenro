import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.black,
      ),
        home: Scaffold(
            body: Container(
          decoration: new BoxDecoration(
              gradient: new LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment(0.8, 0.0),
            colors: [
              const Color(0xffB2DBBF),
              const Color(0xffF3FFBD),
            ],
          )),
          child: Center(
              child: SingleChildScrollView(
            padding: const EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image(image: AssetImage('assets/huella.png')),
                Container(
                  padding: const EdgeInsets.only(top: 30),
                  child: Text(
                    "Login",
                    style: new TextStyle(fontSize: 25),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Correo Electrónico',
                    ),
                    validator: (value) {
                      if (value.length < 3) {
                        return 'Ingrese el correo electrónico del usuario';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                    padding: const EdgeInsets.only(top: 10, bottom: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Contraseña',
                      ),
                      validator: (value) {
                        if (value.length < 3) {
                          return 'Ingrese una Contraseña';
                        } else {
                          return null;
                        }
                      },
                      autofocus: false,
                      obscureText: true,
                    )),
                RaisedButton(
                  onPressed: () {},
                  child: Text('Ingresar', style: TextStyle(fontSize: 20)),
                ),
                FlatButton(
                  child: Text('Registrarse'),
                  onPressed: () =>
                      Navigator.pushReplacementNamed(context, 'register'),
                )
              ],
            ),
          )),
        )));
  }
}
