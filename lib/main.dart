import 'package:flutter/material.dart';
import 'package:primer_intento/page/login.dart';
import 'package:primer_intento/page/register.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String homePage() {
      return 'login';
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: homePage(),
      routes: <String, WidgetBuilder>{
        'login':    (BuildContext context) => Login(),
        'register': (BuildContext context) => Register(),
      },
    );
  }
}
